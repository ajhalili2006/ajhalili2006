# Andrei Jiroh's GitLab README

[![Static Badge](https://img.shields.io/badge/Main%20GitLab%20profile-6E49CB?logo=gitlab&style=for-the-badge)](https://mau.dev/ajhalili2006)
[![Email](https://img.shields.io/badge/Email-EA4335?&style=for-the-badge&logo=Gmail&logoColor=white)](mailto:ajhalili2006@andreijiroh.eu.org)

See also [my main profile README](https://github.com/ajhalili2006/ajhalili2006/blob/ajhalili2006/main/README.md) and [my personal website](https://andreijiroh.eu.org).

## Packages I maintain

* [`github-cli`](https://gitlab.alpinelinux.org/alpine/aports/tree/master/community/github-cli) ([sources](https://github.com/cli/cli), [in the pkg search](https://pkgs.alpinelinux.org/packages?name=github-cli&branch=edge&repo=community&arch=&maintainer=))

## About me

I'm the community package maintainer for GitHub CLI, although I'm open for someone to take over the package maintainership or opt into co-maintainership because I'm been MIA due to school and other personal reasons.

Outside of my regular packaging-related stuff here, I'm currently an student at day and web dev + open-source maintainer at [Recap Time Squad](https://mau.dev/recaptime-dev) (and in my personal projects) at night, and also contributing to other projects in free time.

I'm `ajhalili2006` at mostly all socials (in the fediverse, `@ajhalili2006@tilde.zone`) if you need help with code review and package upgrades.
