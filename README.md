# Hello, world! 👋

> I may be slow to respond to emails and issues since I'm busy at mostly school and personal projects stuff, plus some work at Recap Time Squad (not an company)
> and other family chores, so please send thoughts and prayers to my mental health and sanity at <shitfuckery@bullshit.hq>.[^1]

[![Profile views](https://gpvc.arturio.dev/AJHalili2006)](https://github.com/arturssmirnovs/github-profile-views-counter?ref=ajhalili2006-readme-profile)

![README Banner](./static/readme-banner-2022.png)

I'm Andrei Jiroh, an Filipino 16-year old junior high school student at day and Node.js and Deno backend developer at
[Recap Time Squad](https://gitlab.com/RecapTime) (not an company) and Linux sysadmin-slash-resident Bash script kiddie on personal projects.

## Where to find me elsewhere

> The list may go longer, so check out my [bio.link page](https://ajhalili2006.bio.link) instead.

[![GitLab SaaS](https://img.shields.io/badge/GitLab-6E49CB?&style=for-the-badge&logo=gitlab&logoColor=white&label=%40ajhalili2006%40gitlab.com)](https://gitlab.com/ajhalili2006)
[![GitHub](https://img.shields.io/github/followers/ajhalili2006?label=%40ajhalili2006%40github.com&logo=github&style=for-the-badge)](https://github.com/ajhalili2006)
[![sr.ht](https://img.shields.io/badge/sr.ht-~ajhalili2006-black?style=for-the-badge)](https://sr.ht/~ajhalili2006)
[![Twitter](https://img.shields.io/twitter/follow/Kuys_Potpot?color=blue&label=%40Kuys_Potpot%40twitter.com&logo=twitter&style=for-the-badge)](https://twitter.com/Kuys_Potpot)
[![Reddit](https://img.shields.io/reddit/user-karma/combined/andreihalili?label=andreihalili&logo=reddit&style=for-the-badge)](https://reddit.com/andreihalili)
[![Mastodon](https://img.shields.io/mastodon/follow/000164296?color=blue&domain=https%3A%2F%2Fmastodon.online&label=%40Kuys_Potpot%40mastodon.online&logo=mastodon&style=for-the-badge)](https://mastodon.online/@Kuys_Potpot)
[![Tewlegram](https://img.shields.io/badge/Telegram-grey?&style=for-the-badge&logo=telegram&logoColor=white)](https://telegram.dog/ajhalili2006)
[![Matrix](https://img.shields.io/badge/%40ajhalili2006:envs.net-black?&style=for-the-badge&logo=matrix&logoColor=white)](https://matrix.to/#/@ajhalili2006:envs.net)
[![Keybase](https://img.shields.io/badge/ajhalilidev06-grey?&style=for-the-badge&logo=keybase&logoColor=white)](https://keybase.io/ajhalilidev06)
[![Discord](https://img.shields.io/badge/Discord-5539cc?&style=for-the-badge&logo=discord&logoColor=white)](https://discord.gg/kf5nz4X)

Have any questions about me? I complied [an FAQ](https://ajhalili2006.page.link/personal-faq) for you listing most questions about me,
maybe in the future. For anything related to Recap Time, visit [our contact page](https://recaptime.eu.org/contact).

## Languages and Tools I use

![Terminal](https://img.shields.io/badge/Terminal-4D4D4D?&style=for-the-badge&logo=windowsterminal)
![VS Code](https://img.shields.io/badge/Visual_Studio_Code-007ACC?&style=for-the-badge&logo=visualstudiocode)
![Git](https://img.shields.io/badge/Git-F05032?&style=for-the-badge&logoColor=white&logo=git)
![GitHub](https://img.shields.io/badge/GitHub-181717?&style=for-the-badge&logo=github)
![Node.js](https://img.shields.io/badge/Node.js-339933?&style=for-the-badge&logo=node.js&logoColor=white)
![Gatsby](https://img.shields.io/badge/Gatsby-663399?&style=for-the-badge&logo=gatsby)
![Python](https://img.shields.io/badge/Python-3776AB?&style=for-the-badge&logoColor=white&logo=python)

## My Projects

### Stuff I'm working on

* [Recap Time Squad](https://gitlab.com/RecapTime), (formerly known as The Pins Team) [especially the  Community Radar newsletter](https://gitlab.com/RecapTime/community-radar) - where I mostly do my open-source work there. Here's the tidbits:
  * [Code Server Boilerplates](https://github.com/code-server-boilerplates) - Project Maintainer, collection of ready to made templates for shipping cloud dev environments with code-server pre-installed
  * [Community Lores](https://community-lores.gq) - Maintainer, usually for placing docs stuff in one domain (we do redirection stuff behind the the scenes)
  * [Gitpodified Workspace Images](https://github.com/gitpodify/gitpodified-workspace-images) - an fork of [upstream's workspace images](https://github.com/gitpod-io/workspace-images), implementing fixes and even add some packages.
* [Vaultwarden on PaaS services](https://github.com/RecapTime/vaultwarden-on-paas) - Hard-fork of [bitwardenrs-heroku](https://github.com/std2main/bitwardenrs_heroku) with scripts for automating deployment within minutes, among other features. (now maintained as part of Recap Time Squad's open-source projects)
* GitHub CLI package maintainer for Alpine Linux - version bumps lands on `edge` first, although delays may happen.

### Abandoned Projects

Yeah, press F as many time as you want, and please send thoughts and prayers to these projects to Bull$#!t HQ at <shitfuckery@bullshit.hq>.[^1]

* [Intellivoid SpamProtection API Wrapper for JavaScript](https://github.com/MadeByThePinsHub/Intellivoid-SPB-JS-Wrapper) - due to doing other stuff
* TODO list more abandoned projects here

---

## RSS feeds on personal websites

An feed on personal websites and stuff, showing five latest entries from their own RSS feed.

### Substack Personal Newsletter

<!--SUBSTACK:START-->
TBD
<!--SUBSTACK:END-->

### WordPress Blog

[![WordPress](https://img.shields.io/badge/Blog%20/%20Website-grey?&style=for-the-badge&logo=wordpress&logoColor=white)](https://ajhalili2006.wordpress.com)

<!-- WORDPRESS:START -->
- [July 2022 Status Update](https://ajhalili2006.wordpress.com/2022/08/02/status-update-2022-07/)
- [100 Days of Code 2022 – Week #5](https://ajhalili2006.wordpress.com/2022/03/28/100-days-of-code-2022-week-5/)
- [100 Days of Code 2022 – Week #4](https://ajhalili2006.wordpress.com/2022/03/18/100-days-of-code-2022-week-4/)
- [100 Days of Code 2022 – Week #3](https://ajhalili2006.wordpress.com/2022/03/10/100-days-of-code-2022-week-3/)
- [100 Days of Code 2022 – Week #2](https://ajhalili2006.wordpress.com/2022/03/03/100-days-of-code-week-2/)
<!-- WORDPRESS:END -->

### DevTo

<!-- DEVTO:START -->
- [How do co-authors can sign commits, for vigilant-mode-enabled GitHub users?](https://dev.to/thepinsteam/how-do-co-authors-can-sign-commits-for-vigilant-mode-enabled-github-users-52h2)
- [Introducing Recap Time](https://dev.to/recaptime/introducing-recap-time-4acb)
- [Team Updates — New Twitch channels and more &lpar;oh my!&rpar;](https://dev.to/thepinsteam/team-updates-new-twitch-channels-and-more-oh-my-bc6)
- [Handling requests to delete their account &amp; move to the @ghost account for self-hosted Dev.to instances](https://dev.to/thepinsteam/handling-requests-to-delete-their-account-move-to-the-ghost-account-for-self-hosted-dev-to-instances-5hd9)
<!-- DEVTO:END -->

## My stats and activity stuff

<details>

 <summary>Hidden due to space reasons, click here to expand.</summary>

### LIfetime GH Activity

[![Andrei Jiroh's stats](https://gh-readme-stats-thepinsteam.vercel.app/api?username=ajhalili2006&count_private=true&include_all_commits=true)](https://github.com/anuraghazra/github-readme-stats)

### Speedrun Streak Stats

![Spreedrun Streaks](https://github-readme-streak-stats.herokuapp.com/?user=ajhalili2006&theme=dark)

### Activity Achivements

![Achivements](https://github-profile-trophy.vercel.app/?username=ajhalili2006)

### Top Languages Used

Stats are generated from my personal repos and may not reflect real usage.

[![Top Langs](https://gh-readme-stats-thepinsteam.vercel.app/api/top-langs/?username=ajhalili2006&layout=compact)](https://github.com/anuraghazra/github-readme-stats)


### Latest GitHub Activity

Recent GitHub activity pulled through an CI in an nutshell

<!--START_SECTION:activity-->
1. 💪 Opened PR [#1776](https://github.com/logto-io/logto/pull/1776) in [logto-io/logto](https://github.com/logto-io/logto)
2. 🗣 Commented on [#84](https://github.com/RecapTime/status-page/issues/84) in [RecapTime/status-page](https://github.com/RecapTime/status-page)
3. 🗣 Commented on [#11796](https://github.com/gitpod-io/gitpod/issues/11796) in [gitpod-io/gitpod](https://github.com/gitpod-io/gitpod)
4. 💪 Opened PR [#19](https://github.com/RecapTime/vaultwarden-docker/pull/19) in [RecapTime/vaultwarden-docker](https://github.com/RecapTime/vaultwarden-docker)
5. ❗️ Opened issue [#11796](https://github.com/gitpod-io/gitpod/issues/11796) in [gitpod-io/gitpod](https://github.com/gitpod-io/gitpod)
<!--END_SECTION:activity-->


### Screen Time as an Dev

I'm usually do coding whenever my other self (aka my mind) wants to, or if during the school year, probably on my free time. May not up-to-date as
Gitpod and GitHub Codespaces don't preserve `/home` directories on workspace restarts. Wakatime stats include my coding activity data since April 13, 2021.

[![Wakatime Stuff](https://gh-readme-stats-thepinsteam.vercel.app/api/wakatime?username=ajhalili2006)](https://wakatime.com/ajhalili2006)

</details>

[^1]: Email address doesn't work? See <https://github.com/ajhalili2006/ajhalili2006/blob/master/docs/vocabulary/shitfuckery-at-bullshit-dot-hq.md> why.
