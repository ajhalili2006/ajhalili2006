# Fandoms and Communities outside Open-Source Community

* Animators at Hyun's Dojo Community
  * [Gildedguy (Michael Moy)](https://gildedguy.com) (since 2020)
  * [oxob3000](https://www.youtube.com/channel/UCrtusZ6nfkGMVIql8kE_o7w) (since 2020)
* Generally Speaking: Animators on YouTube
  * [Alan Becker](https://www.youtube.com/channel/UCbKWv2x9t6u8yZoB3KcPtnw) (since 2015)
* Tech
  * Linus Tech Tips
  * The Verge